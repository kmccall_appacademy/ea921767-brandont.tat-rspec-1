def add(value1, value2)
  value1 + value2
end

def subtract(value1, value2)
  value1 - value2
end

def sum(array)
  array.inject(0, :+)
end

def multiply(value1, value2)
  value1 * value2
end

def power(base, power)
  base**power
end

def factorial(value)
  value.downto(1).inject(1, :*)
end
