def echo(phrase)
  phrase
end

def shout(phrase)
  phrase.upcase
end

def repeat(phrase, times = 2)
  repeated_phrase = []
  1.upto(times) { repeated_phrase << phrase }
  repeated_phrase.join(' ')
end

def start_of_word(word, number_letters = 1)
  word[0...number_letters]
end

def first_word(sentence)
  sentence.split(' ')[0]
end

def titleize(sentence)
  little_words = %w(and the over)
  words = sentence.split(' ')
  words.each_with_index.map do |word, i|
    little_words.include?(word) && i != 0 ? word : word.capitalize
  end.join(' ')
end
