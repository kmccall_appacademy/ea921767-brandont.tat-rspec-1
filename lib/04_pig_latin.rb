def translate(sentence)
  words = sentence.split(' ')
  words.map{|word| translate_word(word)}.join(' ')
end

def translate_word(word)
  vowels = %w(a e i o u)
  consonant = ("a".."z").to_a - vowels
    if vowels.include?(word[0])
      "#{word}ay"
    elsif word[0..2].chars.all?{|c| consonant.include?(c)} || word[1..2] == "qu"
      "#{word[3..-1]}#{word[0..2]}ay"
    elsif word[0..1].chars.all?{|c| consonant.include?(c)} || word[0..1] == "qu"
      "#{word[2..-1]}#{word[0..1]}ay"
    else
      "#{word[1..-1]}#{word[0]}ay"
    end
end
